'use strict'

const mongoose = require('mongoose')
const Album = require('./Album')
const Schema = mongoose.Schema;

const ArtistSchema = Schema({
   name: String,
   description: String,
   image: String
})

ArtistSchema.pre('remove', (next) => {
   Album.remove({ artist: this._id }).exec()
   next()
})



module.exports = mongoose.model('Artist', ArtistSchema)

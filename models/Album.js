'use strict'

const mongoose = require('mongoose')
const Song = require('./Song')
const Schema = mongoose.Schema;

const AlbumSchema = Schema({
   title: String,
   description: String,
   year: Number,
   image: String,
   artist: { type: Schema.ObjectId, ref: 'Artist' }
})

AlbumSchema.pre('remove', (next) => {
   Song.remove({ album: this._id }).exec()
   next()
})


module.exports = mongoose.model('Album', AlbumSchema)




'use strict'

const Album = require('../models/Album')
const mongoosePaginate = require('mongoose-pagination')
const uploadImageService = require('../services/upload-image')
const fs = require('fs')
const path = require('path')

function createAlbum(req, res) {
   const params = req.body;
   const album = setDataAlbum(params)

   if (album.title && album.description && album.year && album.image) {
      album.save((err, albumStored) => { // Save para crear un documento en la DB
         if (err) res.status(500).send('Error al crear el album')
         else if (!albumStored) res.status(404).send('No se ha creado el album')
         else res.status(200).send(albumStored)
      })
   } else res.status(200).send('Faltan campos por llenar')
}

function setDataAlbum(params) {
   const album = new Album()
   album.title = params.title
   album.description = params.description
   album.year = params.year
   album.image = 'null'
   album.artist = params.artist
   return album
}

function getAlbum(req, res) {
   const id = req.params.id
   Album.findById(id).populate({ path: 'artist' }).exec((err, album) => { // Asocia el los datos del objeto reacionado dentro del documento
      if (err) res.status(500).send('Error en la peticion')
      else if (!album) res.status(404).send('El album no existe')
      else res.status(200).send(album)
   })

}

function getAllAlbums(req, res) {
   const id = req.params.artist
   let find;

   if (!id) find = Album.find({}).sort('title')
   else find = Album.find({ artist: id }).sort('year')

   find.populate({ path: 'artist' }).exec((err, albums) => {
      if (err) res.status(500).send('Error en la peticion')
      else if (albums.length == 0) res.status(404).send('No hay albumes existentes.')
      else res.status(200).send(albums)
   })
}

function updateAlbum(req, res) {
   const id = req.params.id
   const body = req.body;

   Album.findByIdAndUpdate(id, body, (err, albumUpdated) => {
      if (err) res.status(500).send('Error al actualizar el album')
      else if (!albumUpdated) res.status(404).send('El album no existe')
      else res.status(200).send(albumUpdated)
   })
}

function deleteAlbum(req, res) {
   const id = req.params.id
   Album.findById(id).exec()//Regresa una promesa
      .then(album => {
         if (!album) res.status(404).send('El album no existe')
         else album.remove()
      })
      .then(albumRemoved => res.status(200).send({ albumRemoved: albumRemoved, messsage: 'El album ha sido eliminado.' }))
      .catch(error => res.status(404).send('Error al eliminar el album'))
}

function uploadImage(req, res) {
   uploadImageService.uploadImage(req, Album, 'album')
      .then((artist) => res.status(200).send(artist))
      .catch((error) => res.status(404).send(error))
}

function getImageFile(req, res) {
   const imageFile = req.params.image
   const pathFile = `./uploads/albums/${imageFile}`
   fs.exists(pathFile, (exists) => {
      if (exists) res.sendFile(path.resolve(pathFile))
      else res.status(200).send('No existe la imagen')
   })
}



module.exports = {
   createAlbum,
   getAlbum,
   getAllAlbums,
   updateAlbum,
   deleteAlbum,
   uploadImage,
   getImageFile
}
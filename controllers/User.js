'user strict'

const bcrypt = require('bcrypt-nodejs')
const User = require('../models/User') // Importa modelo de Mongoose
const jwt = require('../services/jwt')
const uploadImageService = require('../services/upload-image')
const fs = require('fs')
const path = require('path')

function createUser(req, res) {
	const params = req.body;
	const user = setDataUser(params)

	if (user.name && user.surname && user.email && user.password) {
		bcrypt.hash(user.password, null, null, (err, hash) => { // Metodo hash para encriptar la contraseña
			if (!err) {
				user.password = hash
				user.save((err, userStored) => { // Save para crear un documento en la DB
					if (err) res.status(500).send('Error al crear el usuario')
					else if (!userStored) res.status(404).send('No se ha creado el usuario')
					else res.status(200).send(userStored)
				})
			}
		})
	} else res.status(200).send('Faltan campos por llenar')
}

function setDataUser(params) {
	const user = new User()
	user.name = params.name;
	user.surname = params.surname;
	user.email = params.email
	user.password = params.password
	user.role = 'ROLE_USER'
	user.image = 'null'
	return user
}

function getUser(req, res) {
	const params = req.body
	const email = params.email
	const password = params.password

	User.findOne({ email: email.toLowerCase() }, (err, user) => { // FindOne para buscar un registro especifico en la DB
		if (err) res.status(500).send('Error en la petición')
		else if (!user) res.status(404).send('Usuario no existe')
		else {
			bcrypt.compare(password, user.password, (err, check) => { // Compara contraseñas
				if (check) {
					if (params.gethash) res.status(200).send({ token: jwt.createToken(user) })  //DEvolver token
					else res.status(200).send(user)
				} else {
					res.status(404).send('Contraseña invalida')
				}
			})
		}
	})
}

function updateUser(req, res) {
	const id = req.params.id
	const body = req.body;

	User.findByIdAndUpdate(id, body, (err, userUpdated) => {
		if (err) res.status(500).send('Error al actualizar el usuario')
		else if (!userUpdated) res.status(404).send('El usuario no existe')
		else res.status(200).send(userUpdated)
	})
}

function uploadImage(req, res) {
	uploadImageService.uploadImage(req, User, 'usuario')
		.then((imageUser) => res.status(200).send({ 'image': imageUser.pathFile, 'user': imageUser.objectUpdated }))
		.catch((error) => res.status(404).send(error))
}

function getImageFile(req, res) {
	const imageFile = req.params.image
	const pathFile = `./uploads/users/${imageFile}`
	fs.exists(pathFile, (exists) => {
		if (exists) res.sendFile(path.resolve(pathFile))
		else res.status(200).send('No existe la imagen')
	})
}

module.exports = {
	createUser,
	getUser,
	updateUser,
	uploadImage,
	getImageFile
}
'use strict'

const Artist = require('../models/Artist')
const mongoosePaginate = require('mongoose-pagination')
const uploadImageService = require('../services/upload-image')
const fs = require('fs')
const path = require('path')

function createArtist(req, res) {
   const params = req.body;
   const artist = setDataArtist(params)

   if (artist.name && artist.description && artist.image) {
      artist.save((err, artistStored) => { // Save para crear un documento en la DB
         if (err) res.status(500).send('Error al crear el artista')
         else if (!artistStored) res.status(404).send('No se ha creado el artista')
         else res.status(200).send(artistStored)
      })
   } else res.status(200).send('Faltan campos por llenar')
}

function setDataArtist(params) {
   const artist = new Artist()
   artist.name = params.name
   artist.description = params.description
   artist.image = 'null'
   return artist
}

function getArtist(req, res) {
   const id = req.params.id
   Artist.findById(id, (err, artist) => { // Consulta por id a DB
      if (err) res.status(500).send('Error en la peticion')
      else if (!artist) res.status(404).send('El artista no existe')
      else res.status(200).send(artist)
   })
}

function getAllArtists(req, res) {
   const page = (req.params.page) ? req.params.page : 1
   const itemsPerPage = 3;
   Artist.find().sort('name').paginate(page, itemsPerPage, (err, artists, totalItems) => { //Trae todo los registros de la tabla de la DB
      if (err) res.status(500).send('Error en la peticion')
      else if (!artists) res.status(500).send('No hay artistas')
      else res.status(200).send({ totalItems: totalItems, artists: artists })
   })
}

function updateArtist(req, res) {
   const id = req.params.id
   const body = req.body;

   Artist.findByIdAndUpdate(id, body, (err, artistUpdated) => {
      if (err) res.status(500).send('Error al actualizar el artista')
      else if (!artistUpdated) res.status(404).send('El artista no existe')
      else res.status(200).send(artistUpdated)
   })
}

function deleteArtist(req, res) {
   const id = req.params.id
   Artist.findById(id).exec()//Regresa una promesa
      .then(artist => {
         if (!artist) res.status(404).send('El artista no existe')
         else artist.remove()
      })
      .then(artisteRemoved => res.status(200).send({ artistRemoved: artisteRemoved, messsage: 'El artista ha sido eliminado.' }))
      .catch(error => res.status(404).send('Error al eliminar el artista'))
}

function uploadImage(req, res) {
   uploadImageService.uploadImage(req, Artist, 'artista')
      .then((artist) => res.status(200).send(artist))
      .catch((error) => res.status(404).send(error))
}

function getImageFile(req, res) {
   const imageFile = req.params.image
   const pathFile = `./uploads/artists/${imageFile}`
   fs.exists(pathFile, (exists) => {
      if (exists) res.sendFile(path.resolve(pathFile))
      else res.status(200).send('No existe la imagen')
   })
}



module.exports = {
   getArtist,
   createArtist,
   getAllArtists,
   updateArtist,
   deleteArtist,
   uploadImage,
   getImageFile
}
'use strict'

const Song = require('../models/Song')
const mongoosePaginate = require('mongoose-pagination')
const uploadImageService = require('../services/upload-image')
const fs = require('fs')
const path = require('path')

function createSong(req, res) {
   const params = req.body;
   const song = setDataSong(params)

   if (song.number && song.name && song.file && song.album) {
      song.save((err, songStored) => { // Save para crear un documento en la DB
         if (err) res.status(500).send('Error al crear la cancion')
         else if (!songStored) res.status(404).send('No se ha creado la cancion')
         else res.status(200).send(songStored)
      })
   } else res.status(200).send('Faltan campos por llenar')
}

function setDataSong(params) {
   const song = new Song()
   song.number = params.number
   song.name = params.name
   song.file = "null"
   song.album = params.album
   return song
}

function getSong(req, res) {
   const id = req.params.id
   Song.findById(id).populate({ path: 'album' }).exec((err, song) => { // Consulta por id a DB
      if (err) res.status(500).send('Error en la peticion')
      else if (!song) res.status(404).send('La cancion no existe')
      else res.status(200).send(song)
   })
}


function getAllSongs(req, res) {
   const id = req.params.album
   let find;

   if (!id) find = Song.find({}).sort('name')
   else find = Song.find({ album: id }).sort('number')

   find.populate({ path: 'album', populate: { path: 'artist', model: 'Artist' } }).exec((err, canciones) => {
      if (err) res.status(500).send('Error en la peticion')
      else if (canciones.length == 0) res.status(404).send('No hay canciones existentes.')
      else res.status(200).send(canciones)
   })
}


function updateSong(req, res) {
   const id = req.params.id
   const body = req.body;

   Song.findByIdAndUpdate(id, body, (err, songUpdated) => {
      if (err) res.status(500).send('Error al actualizar la cancion')
      else if (!songUpdated) res.status(404).send('la cancion no existe')
      else res.status(200).send(songUpdated)
   })
}

function deleteSong(req, res) {
   const id = req.params.id
   Song.findByIdAndRemove(id, (err, songRemoved) => {
      if (err) res.status(500).send('Error al eliminar la cancion')
      else if (!songRemoved) res.status(404).send('la cancion no existe')
      else res.status(200).send(songRemoved)
   })
}

function uploadSong(req, res) {

   const id = req.params.id
   let nameFile = 'No subido'
   if (Object.keys(req.files).length > 0) {
      const pathFile = req.files.file.path.split('/')[2]
      const extension = pathFile.split('.')[1]
      if (extension == 'mp3' || extension == 'mp4') {
         Song.findByIdAndUpdate(id, { file: pathFile }, (err, songUpdate) => {
            if (err) res.status(500).send(`Error al subir la cancion`)
            else if (!songUpdate) res.status(404).send(`No se ha podido actualizar la cancion`)
            else res.status(200).send(songUpdate)
         })
      } else {
         deleteFromDisk(req.files.image.path)
         res.status(404).send(`Extension del archivo no valida`)
      }
   } else reject('No se ha subido ninguna imagen')

}

function deleteFromDisk(pathFile) {
   fs.unlinkSync(pathFile);
   console.log('successfully deleted', pathFile);
}

function getFile(req, res) {
   const file = req.params.file
   const pathFile = `./uploads/songs/${file}`
   fs.exists(pathFile, (exists) => {
      if (exists) res.sendFile(path.resolve(pathFile))
      else res.status(200).send('No existe la cancion')
   })
}



module.exports = {
   createSong,
   getSong,
   getAllSongs,
   updateSong,
   deleteSong,
   uploadSong,
   getFile
}
const fs = require('fs')

exports.uploadImage = function (req, object, collection) {
   return new Promise((resolve, reject) => {
      const id = req.params.id
      let nameFile = 'No subido'
      if (Object.keys(req.files).length > 0) {
         const pathFile = req.files.image.path.split('/')[2]
         const extension = pathFile.split('.')[1]
         if (extension == 'png' || extension == 'jpg' || extension == 'gif') {
            object.findByIdAndUpdate(id, { image: pathFile }, (err, objectUpdated) => {
               if (err) reject(`Error al subir la imagen del ${collection}`)
               else if (!objectUpdated) reject(`No se ha podido actualizar la imagen del ${collection}`)
               else resolve({ objectUpdated, pathFile })
            })
         } else {
            deleteFromDisk(req.files.image.path)
            reject('Extension del archivo no valida')
         }
      } else reject('No se ha subido ninguna imagen')
   })
}

function deleteFromDisk(pathFile) {
   fs.unlinkSync(pathFile);
   console.log('successfully deleted', pathFile);
}
'use strict'

const jwt = require('jwt-simple')
const moment = require('moment')
const secret = 'clave_secreta_user'

exports.createToken = function (user) {
   let payload = {
      sub: user._id, // Sub para asociar el id del usuario
      name: user.name,
      surname: user.surname,
      email: user.email,
      password: user.password,
      role: user.role,
      image: user.image,
      iat: moment().unix(), // Fecha creacion token en timestamp
      exp: moment().add(30, 'days').unix() // Fecha expiracion token en timestamp
   }
   return jwt.encode(payload, secret)
}

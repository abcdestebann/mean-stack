'user strict'

const express = require('express')
const SongController = require('../controllers/Song')
const middlewareAuth = require('../middlewares/authenticated')
const multiparty = require('connect-multiparty')
const middlewareUpload = multiparty({ uploadDir: './uploads/songs' })
const api = express.Router()

api.post('/createSong', middlewareAuth.ensureAuth, SongController.createSong)
api.get('/getSong/:id', middlewareAuth.ensureAuth, SongController.getSong)
api.get('/getAllSongs/:album?', middlewareAuth.ensureAuth, SongController.getAllSongs)
api.put('/updateSong/:id', middlewareAuth.ensureAuth, SongController.updateSong)
api.delete('/deleteSong/:id', middlewareAuth.ensureAuth, SongController.deleteSong)
api.post('/uploadSong/:id', [middlewareAuth.ensureAuth, middlewareUpload], SongController.uploadSong)
api.get('/getFile/:file', SongController.getFile)

module.exports = api
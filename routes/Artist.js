'user strict'

const express = require('express')
const ArtistController = require('../controllers/Artist')
const middlewareAuth = require('../middlewares/authenticated')
const multiparty = require('connect-multiparty')
const middlewareUpload = multiparty({ uploadDir: './uploads/artists' })
const api = express.Router()

api.post('/createArtist', middlewareAuth.ensureAuth, ArtistController.createArtist)
api.get('/getArtist/:id', middlewareAuth.ensureAuth, ArtistController.getArtist)
api.get('/getAllArtists/:page?', middlewareAuth.ensureAuth, ArtistController.getAllArtists)
api.put('/updateArtist/:id', middlewareAuth.ensureAuth, ArtistController.updateArtist)
api.delete('/deleteArtist/:id', middlewareAuth.ensureAuth, ArtistController.deleteArtist)
api.post('/uploadImageArtist/:id', [middlewareAuth.ensureAuth, middlewareUpload], ArtistController.uploadImage)
api.get('/getImageArtist/:image', ArtistController.getImageFile)

module.exports = api
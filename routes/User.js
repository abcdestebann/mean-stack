'user strict'

const express = require('express')
const UserController = require('../controllers/User')
const middlewareAuth = require('../middlewares/authenticated')
const multiparty = require('connect-multiparty')
const middlewareUpload = multiparty({ uploadDir: './uploads/users' })
const api = express.Router()

api.post('/createUser', UserController.createUser)
api.post('/getUser', UserController.getUser)
api.put('/updateUser/:id', middlewareAuth.ensureAuth, UserController.updateUser)
api.post('/uploadImageUser/:id', [middlewareAuth.ensureAuth, middlewareUpload], UserController.uploadImage)
api.get('/getImageUser/:image', UserController.getImageFile)

module.exports = api
'user strict'

const express = require('express')
const AlbumController = require('../controllers/Album')
const middlewareAuth = require('../middlewares/authenticated')
const multiparty = require('connect-multiparty')
const middlewareUpload = multiparty({ uploadDir: './uploads/albums' })
const api = express.Router()

api.post('/createAlbum', middlewareAuth.ensureAuth, AlbumController.createAlbum)
api.get('/getAlbum/:id', middlewareAuth.ensureAuth, AlbumController.getAlbum)
api.get('/getAllAlbums/:artist?', middlewareAuth.ensureAuth, AlbumController.getAllAlbums)
api.put('/updateAlbum/:id', middlewareAuth.ensureAuth, AlbumController.updateAlbum)
api.delete('/deleteAlbum/:id', middlewareAuth.ensureAuth, AlbumController.deleteAlbum)
api.post('/uploadImageAlbum/:id', [middlewareAuth.ensureAuth, middlewareUpload], AlbumController.uploadImage)
api.get('/getImageAlbum/:image', AlbumController.getImageFile)

module.exports = api
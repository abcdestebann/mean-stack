'use strict'

const mongoose = require('mongoose')
const app = require('./app')
const port = process.env.PORT || 3977

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/mean-udemy', (err, res) => {
   if (err) throw err
   else {
      console.log('Base de datos conectada!');
      app.listen(port, () => {
         console.log(`Listening on port http://localhost:${port}`)
      })
   }
})


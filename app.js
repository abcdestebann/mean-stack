'user strict'

const express = require('express')
const bodyParser = require('body-parser')

const app = express()

// Cargar Rutas 
const userRoutes = require('./routes/User')
const artistRoutes = require('./routes/Artist')
const albumRoutes = require('./routes/Album')
const songRoutes = require('./routes/Song')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use((req, res, next) => {
   res.header('Access-Control-Allow-Origin', '*');
   res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-Width, Content-Type, Accept, Access-Control-Allow-Request-Method');
   res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
   res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE')

   next()
})

// Routes
app.use('/api', userRoutes)
app.use('/api', artistRoutes)
app.use('/api', albumRoutes)
app.use('/api', songRoutes)



module.exports = app
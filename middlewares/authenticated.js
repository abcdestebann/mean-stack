'use strict'

const jwt = require('jwt-simple')
const moment = require('moment')
const secret = 'clave_secreta_user'

exports.ensureAuth = function (req, res, next) {
   if (!req.headers.authorization) return res.status(403).send('La petición no tiene el header de autenticacion')
   const token = req.headers.authorization.replace(/['"]+/g, '')
   try {
      var payload = jwt.decode(token, secret)
      if (payload.exp <= moment().unix()) return res.status(401).send('Token ha expirado')
   } catch (ex) {
      return res.status(404).send('Token no valido')
   }
   req.user = payload
   next()
} 